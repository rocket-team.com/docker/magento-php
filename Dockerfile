FROM php:7.3-fpm-alpine

MAINTAINER Oleksii Stepanov <sagerbox@gmail.com>

RUN apk add --no-cache libzip freetype libpng libjpeg-turbo libxslt icu-libs git patch ssmtp && \
  apk add --no-cache --virtual .builds_deps  freetype-dev libpng-dev libjpeg-turbo-dev libzip-dev libxslt-dev icu-dev && \
  docker-php-ext-configure gd \
    --with-gd \
    --with-freetype-dir=/usr/include/ \
    --with-png-dir=/usr/include/ \
    --with-jpeg-dir=/usr/include/ && \
  NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) && \
  docker-php-ext-install -j${NPROC} gd zip xsl soap pdo_mysql intl bcmath opcache && \
  apk del --no-cache .builds_deps && \
  curl https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer && \
  echo "memory_limit = -1" >> /usr/local/etc/php/php.ini && \
  echo "sendmail -i -t" >> /usr/local/etc/php/php.ini
